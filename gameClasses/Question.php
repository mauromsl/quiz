<?php

/*
 *Question
 *
 * Class that defines the question objects to be used on tue quiz
 * 
 */

namespace gameClasses;

class Question
{
	private $question;
	private $answers;
	private $correctAnswer;

	public function __construct($question, $answers, $correctAnswer){

		$this->question = $question;
		foreach ($answers as $key => $value) {
			$this->answers[] = $value;
		}
		$this->correctAnswer = $correctAnswer;
	}

	/**
	*
	* Returns all object variables in an array like 'variableName => value'
	*
	*/

	public function getArray(){

		return get_object_vars($this);
	}
}

?>