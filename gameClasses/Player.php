<?php

namespace gameClasses;

/*
 *Player
 *
 * Class that defines the user objects
 * 
 */

class Player
{
	protected $username;
	protected $password;
	protected $email;
	protected $admin;

	/**
	*
	* Constructor
	*
	*/

	public function __construct($username, $password, $email) {

		$this->username = $username;
		$this->password = $password;
		$this->email = $email;
		$this->admin = false;
	}


	/**
	*
	* Returns the username
	*
	*/


	public function getUsername(){
		return $this->username;
	}


	/**
	*
	* Returns all object variables in an array like 'variableName => value'
	*
	*/

	public function getArray(){
		return get_object_vars($this);
	}
}