<?php

/*
 *Player
 *
 * Class that defines the game instances
 * 
 */


namespace gameClasses;

class Game
{
	private $player;
	private $difficulty;
	private $questions;
	private $playerAnswers;
	private $start;
	private $end;
	private $finalScore;
	private $time;

	/**
	*
	* Constructor
	*
	*/

	public function __construct($player, $difficulty, $questions){

		$this->player = $player;
		$this->difficulty = $difficulty;
		$this->questions = $questions;
		$this->start = microtime(true);
		$this->end = false;
	}

	/**
	*
	* Returns all object variables in an array like 'variableName => value'
	*
	*/

	public function getArray(){

		return get_object_vars($this);
	}

}

?>