<?php

use gameClasses\Question;

class QuestionTest extends PHPUnit_Framework_TestCase
{

	public $question;

	public function testQuestionCreation(){
		$this->question = new Question(array('q1','q2'), array('1','2'), 'test');

		$this->assertTrue($this->question->getArray()['correctAnswer'] === 'test');
	}
}


?>
