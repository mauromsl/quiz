<?php

use gameClasses\Player;

class PlayerTest extends PHPUnit_Framework_TestCase
{

	public $player;

	public function testPlayerCreation(){
		$this->player = new Player('username', 'password', 'email@email.com');

		$this->assertTrue($this->player->getUsername() === 'username');
	}
}


?>
