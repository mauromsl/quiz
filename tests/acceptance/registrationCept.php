<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('Register user');
$I->expect('to be promted an error, username not available');
$I->amOnPage('/index.php/register');
$I->see('Username'); 
$I->fillField('username', 'mauro');
$I->fillField('password', 'password');
$I->fillField('email', 'a@a.a');
$I->click('login');
$I->see('The username you entered has been used already'); 
?>