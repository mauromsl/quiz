<?php

//My own classes loaded with autoloader:
require_once __DIR__.'/vendor/autoload.php';
use gameClasses\Player;
use gameClasses\Game;
use gameClasses\Question;

//My own reusable mongo functions :
require_once __DIR__.'/gameFunctions/mongoQuery.php';
require_once __DIR__.'/gameFunctions/mongoInsert.php';
require_once __DIR__.'/gameFunctions/mongoUpdate.php';
require_once __DIR__.'/gameFunctions/mongoRemove.php';
require_once __DIR__.'/gameFunctions/mongo5Questions.php';
require_once __DIR__.'/gameFunctions/registrationEmail.php';


//Symfony components
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Debug\ErrorHandler;
	ErrorHandler::register();

// application instance
$app = new Silex\Application();
$app['debug'] = true;

//Twig service
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));
$app->register(new Silex\Provider\ValidatorServiceProvider());

$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    $twig->addGlobal('session', $_SESSION);
    return $twig;
}));


//sessions
session_start();
$app->before(function (Request $request, $app) {
	if(!isset($_SESSION['username'])) {
		$_SESSION['username'] = 0;
	}
});

//route before methods
$login_required = function (Request $request, $app) {
	/*
	* checks if the session is set for required views
	*/
    if(!is_string($_SESSION['username'])) {
		return $app->redirect('/~u1340677/quiz/index.php');
	}
};

$no_login_required = function (Request $request, $app) {
	/*
	* checks if the session is not set for login and registration views
	*/
    if(is_string($_SESSION['username'])) {
		return $app->redirect('/~u1340677/quiz/index.php/play');
	}
};




//Routes:

############################################        Route index       ####################################################
$app->get('/', function () use ($app) {
    
    //get the top 3 score games, sorted by descending score and ascending time
	$games = mongoQuery('Games', array('end'=>array('$gt' => 0)));
	$games->sort(array('finalScore'=>-1, 'time'=>1))->limit(3);

	$context['message'] = "Welcome, My name is Llegion and I am the quiz master <br> Log in or register to start your game... <br> ...if you dare";
	$context['first'] = $games->getNext();
	$context['second'] = $games->getNext();
	$context['third'] = $games->getNext();
    return $app['twig']->render('components/login.twig',$context);
})->before($no_login_required);

############################################       Route register       #################################################


$app->get('/register', function () use ($app) {
	$context['message'] = "Fill the registration form.<br> Use your UEL email (if you have one)";
    return $app['twig']->render('components/register.twig',$context);
})->before($no_login_required);

############################################      Route Registration    ################################################# 

$app->post('/registration', function (request $request) use ($app) {

	//check if player already exists in db
	$player = mongoQuery('Players', array('username'=>$request->get('username')));
	if($player) {
		$context['message'] = "<h3>ERROR</h3> The username you entered has been used already. Use a different one";
		return $app['twig']->render('components/register.twig',$context);
	}

	$player = new Player($request->get('username'), md5($request->get('password')), filter_var($request->get('email'), FILTER_SANITIZE_EMAIL));
	mongoInsert('Players', $player->getArray());
    $email = registrationEmail($player->getArray());
    $context['message'] = "Welcome ".$player->getUsername().", please use your credentials to log in<br>";
    $context['first'] = false;
    return $app['twig']->render('components/login.twig',$context);
});

############################################          Route login        ############################################### 

$app->post('/login', function (request $request) use ($app) {

	
	$player = mongoQuery('Players', array('username'=>$request->get('username')));
	$context['first'] = false;
	if($player) {
		if($player['password'] == md5($request->get('password'))) {
			$_SESSION['username'] = $request->get('username');
			$context['message'] = "Login Succesful";
			return $app['twig']->render('components/game_start.twig',$context);
		}
		else {
			$context['message'] = "<h3>ERROR</h3> This username and password combination don't match";
			return $app['twig']->render('components/login.twig',$context);
		}

	}
	else {

		$context['message'] = "<h3>ERROR</h3> The username you entered does not exist";
		return $app['twig']->render('components/login.twig',$context);
	}



	    return $app['twig']->render('components/login.twig',$context);
});

############################################      Route logout   ####################################### 

$app->get('/logout', function (request $request) use ($app) {
	$_SESSION['username'] = 0;
	$context['message'] = "<h3>Bye, Bye...</h3> Be good out there..";
	$context['first'] = false;
	return $app['twig']->render('components/login.twig',$context);
});

############################################      Route New Game    #########################################

$app->post('/new_game', function(request $request) use ($app) {

	//Remove unfinished games
	mongoRemove('Games', array('player'=> $_SESSION['username'], 'end'=>false));
	//Query 5 random questions
	$all_questions = mongoQuery('Questions', false);
	$questions = mongo5Questions($all_questions);

	$game = new Game($_SESSION['username'], $request->get('difficulty'), $questions);
	mongoInsert('Games', $game->getArray());
	

	return $app->redirect('play');

})->before($login_required);

############################################      Route Play      #########################################

$app->get('/play', function(request $request) use ($app) {

	//Find game user game in progress
	$game = mongoQuery('Games', array('player'=> $_SESSION['username'], 'end'=>false));
	if ($game) {
		//find total questions answered so far
		$last_question = count($game['playerAnswers']);
		if ($last_question == 5) {
			//5 answers means game is finished
			$end = microtime(true);

			$correctAnswers = 0;
			for ($i=0; $i < 5; $i++) { 
				if ($game['questions'][$i]['correctAnswer'] == $game['playerAnswers'][$i]){
					$correctAnswers++;
				}
			}
			if ($game['difficulty'] === 'normal'){
				$finalScore = $correctAnswers * 50;
			}
			else{
				$finalScore = ($correctAnswers * 100) - ((5 - $correctAnswers) *50);
			}
			$time = $end - $game['start'];
			mongoUpdate('Games', $game, array('$set'=>array('end'=>$end,'time'=>$time, 'finalScore'=>$finalScore)));
			$sec = intval($time);
			$micro = $time - $sec;
			$time = strftime('%T', mktime(0, 0, $sec)) . str_replace('0.', '.', sprintf('%.3f', $micro));
			$context['message'] = '<h3> Finished!</h3> At least you made it through...<br> This are your results';
			$context['results'] = array('time'=>$time, 'correctAnswers'=>$correctAnswers, 'score'=>$finalScore);
			return $app['twig']->render('components/score.twig',$context);
		}
		elseif ($last_question < 5) {
			if (preg_match('/^[0-3]{1}$/',$request->get('player_answer'))) {
				$playerAnswers= $game['playerAnswers'];
				$playerAnswers[]= $request->get('player_answer');
				mongoUpdate('Games', $game, array('$set'=>array('playerAnswers'=>$playerAnswers)));

				return $app->redirect('play');
			}

			$next_question = $game['questions'][$last_question];
			$context['message'] = $next_question['question'];
			$context['answers'] = $next_question['answers'];
			return $app['twig']->render('components/play.twig',$context);
		}
		// If more than 5 answers user has cheated.
		else {
			$_SESSION['username'] = 0;
			$context['message'] = '<h3>HACKING DETECTED ... </h3> Launching self-defense mechanism... <p> User kicked out</p><p> Bye, loser</p>';
			return $app['twig']->render('components/login.twig', $context);
		}
	}
	else {
		$context['message'] = "Select game mode";
		return $app['twig']->render('components/game_start.twig',$context);
	}

})->before($login_required);


############################################      Route Admin      ######################################

$app->get('/admin', function() use ($app) {
	$player = mongoQuery('Players', array('username'=>$_SESSION['username']));
	if ($player['admin'] === true) {
		$context['message'] = '<h3>Admin status detected ...</h3> Use the form to add a new question.<p> There is no input validation, please behave yourself!';
		return $app['twig']->render('components/admin.twig', $context);
	}

	$_SESSION['username'] = 0;
	$context['message'] = '<h3>Admin status not detected ...</h3> Launching self-defense mechanism... <p> User kicked out</p><p> Bye, loser</p>';
	return $app['twig']->render('components/login.twig', $context);

});


############################################       Route Add Question    ###############################

$app->post('/add_question', function(request $request) use ($app){

	$question = new Question(
		$request->get('question'), 
		array(
			$request->get('answer1'),
			$request->get('answer2'),
			$request->get('answer3'),
			$request->get('answer4')
		),
		$request->get('correct_answer')
	);

	mongoInsert('Questions', $question->getArray());
	$context['message'] = '<h3>Question saved in mongoDB</h3> Fill the form to add another one.</p>';
	return $app['twig']->render('components/admin.twig', $context);

});


############################################     Show database     #####################################
$app->get('/database', function () use ($app) {

	$players = mongoQuery('Players', false);

	foreach ($players as $key => $value) {
		 {
			echo $value['username']."<br>";
		}
		echo "<hr>";
	}

	echo "<h1>GAMES</h1> <hr>";
	$games = mongoQuery('Games', false);

	foreach ($games as $key => $value) {
		print_r($value);
		echo "<hr>";
	}

	echo "<h1>QUESTIONS</h1> <hr>";
	$games = mongoQuery('Questions', false);

	foreach ($games as $key => $value) {
		print_r($value['question']);
		echo "<hr>";
	}	

    return '<h1>done</h1>';
})->before($login_required);



$app->run();
