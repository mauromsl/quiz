<?php

/*
 *
 * Given all the questions in the database, it returns 5 randomly
 * 
 * 
 *
 * @param array $all_questions contains all the questions from where the random ones will be picked
 * 
 */



function mongo5Questions($all_questions){

	//skip a random number of questions but ensuring that there are 5 remaining
	$cursor[0] = $all_questions->limit(5)->skip(mt_rand(0, ($all_questions->count())-5))->getNext();

	for ($i=1; $i < 5; $i++) { 
		$cursor[$i] = $all_questions->getNext();

	}

	foreach ($cursor as $key => $value) {
		$questions[] = $value;
	}

	return $questions;
}