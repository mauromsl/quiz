<?php
/*
 *
 * Given a collection, removes all documents from it.
 * If an array is given that matches anu documents it will remove does instead
 * 
 * @param string $collection the collection name
 * @param array $array contains the criteria of the documents to be deleted
 *
 */

function mongoRemove($collection, $array){
	$connection = new MongoClient("mongodb://u1340677:1360fbf3@localhost/u1340677");
	$collection = $connection->u1340677->$collection;

	// find document by given collection and criteria in the arguments.
	if ($array){
		$result = $collection->remove($array);

	}
	// if no arguments removes the whole collection.
	else {
		$result = $collection->remove();
	}

	return $result;
}