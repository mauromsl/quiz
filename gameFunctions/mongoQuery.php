<?php


/*
 *
 * Given a collection, queries all documents from it.
 * If an array is given that matches anu documents it will query does instead
 * 
 * @param string $collection the collection name
 * @param array $array contains the criteria of the documents to be deleted
 *
 */


function mongoQuery($collection, $array){
	$connection = new MongoClient("mongodb://u1340677:1360fbf3@localhost/u1340677");
	$collection = $connection->u1340677->$collection;

	// find document by given collection and criteria in the arguments.
	if ($array){
		$result = $collection->find($array);
		if ($result->count() < 2){
			$result = $collection->findOne($array);
		}

	}
	// if no arguments returns the whole collection.
	else {
		$result = $collection->find();
	} 

	return $result;
}
